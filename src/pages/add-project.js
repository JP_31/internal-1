import React from 'react';
import { Link } from "react-router-dom";
import { Layout, Form } from 'antd';
import SideBar from '../components/Sidebar';
import EditableTable from '../components/Editabletable';

const { Content } = Layout;
class SiderDemo extends React.Component {
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <SideBar />
        <Layout>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            <EditableTable />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

const Addproject = Form.create({ name: 'addproject' })(SiderDemo);

export default Addproject