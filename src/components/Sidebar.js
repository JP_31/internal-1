import React from 'react';
import { Link } from "react-router-dom";
import { Layout, Menu, Icon } from 'antd';

import user from '../logo.png';


const { Sider } = Layout;

class SideBar extends React.Component {

    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Sider
                breakpoint="xs"
                collapsible collapsed={this.state.collapsed} onCollapse={this.toggle}>
                <img alt="no logo" src={user} className="logo" />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1">
                        <Link to="/home" ><Icon type="dashboard" />
                            <span>Dashboard</span></Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                    <Link to="/projects" >
                        <Icon type="idcard" />
                        <span>Projects</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Link to="/users" ><Icon type="user" />
                            <span>User</span></Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}

export default SideBar