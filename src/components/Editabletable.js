import React from 'react';
import { Link } from "react-router-dom";
import { Layout, Form, Input, Button, Table, Popconfirm } from 'antd';
import { Grid, Row, Col } from 'react-flexbox-grid';

const { Content } = Layout;
const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  state = {
    editing: false,
  };

  toggleEdit = () => {
    const editing = !this.state.editing;
    this.setState({ editing }, () => {
      if (editing) {
        this.input.focus();
      }
    });
  };

  save = e => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      if (error && error[e.currentTarget.id]) {
        return;
      }
      this.toggleEdit();
      handleSave({ ...record, ...values });
    });
  };

  renderCell = form => {
    this.form = form;
    const { children, dataIndex, record, title } = this.props;
    const { editing } = this.state;
    return editing ? (
      <Form.Item style={{ margin: 0 }}>
        {form.getFieldDecorator(dataIndex, {
          rules: [
            {
              required: true,
              message: `${title} is required.`,
            },
          ],
          initialValue: record[dataIndex],
        })(<Input ref={node => (this.input = node)} onPressEnter={this.save} onBlur={this.save} />)}
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{ paddingRight: 24 }}
          onClick={this.toggleEdit}
        >
          {children}
        </div>
      );
  };

  render() {
    const {
      editable,
      dataIndex,
      title,
      record,
      index,
      handleSave,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editable ? (
          <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
        ) : (
            children
          )}
      </td>
    );
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'Project Name',
        dataIndex: 'projectName',
        width: '15%',
        editable: true,
      },
      {
        title: 'Client Name',
        dataIndex: 'clientName',
        width: '15%',
        editable: true,
      },
      {
        title: 'Contact Person',
        dataIndex: 'cPerson',
        width: '15%',
        editable: true,
      },
      {
        title: 'Developer',
        dataIndex: 'developer',
        width: '15%',
        editable: true,
      },
      {
        title: 'Date Started',
        dataIndex: 'dateStarted',
        width: '15%',
        editable: true,
      },
      {
        title: 'Date Completion',
        dataIndex: 'dateCompletion',
        width: '15%',
        editable: true,
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Link to="/view-indiv-project">View</Link>
          ) : null,
      },
      {
        title: 'operation',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];

    this.state = {
      dataSource: [
        {
          key: '0',
          projectName: 'ZDRC Website',
          clientName: 'ZDRC / Miss Kim',
          cPerson: 'Miss Kim',
          developer: 'Insert Name Here',
          dateStarted: '08/29/2019',
          dateCompletion: '12/12/2019',
        },
        {
          key: '1',
          projectName: 'ZDRC Website',
          clientName: 'ZDRC / Miss Kim',
          cPerson: 'Miss Kim',
          developer: 'Insert Name Here',
          dateStarted: '08/29/2019',
          dateCompletion: '12/12/2019',
        },
      ],
      count: 2,
    };
  }

  handleDelete = key => {
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
  };

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      projectName: `Project Name`,
      clientName: `Client Name`,
      cPerson: `Contact Person`,
      developer: `Developer Name`,
      dateStarted: `00-00-0000`,
      dateCompletion: `00-00-0000`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };

  handleSave = row => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
  };

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <Button onClick={this.handleAdd} type="primary" style={{ marginBottom: 16 }}>
              Add Project</Button>
          </Col>
          <Col xs={12}>
            <Table
              components={components}
              rowClassName={() => 'editable-row'}
              bordered
              dataSource={dataSource}
              columns={columns}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default EditableTable