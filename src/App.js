
import React from 'react';
import { BrowserRouter as Router, Route,Switch } from "react-router-dom"; 
import 'antd/dist/antd.css';
import './App.css';
import NormalLoginForm from "./pages/login";
import Dashboard from "./pages/main-dashboard";
import Addproject from "./pages/add-project";
import Adduser from "./pages/add-user";
import Addscope from "./pages/add-scope";
import ViewIndivProj from "./pages/view-indiv-project";


function App() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" name="login" component={NormalLoginForm} />
          <Route exact path="/home" name="home" component={Dashboard} />
          <Route exact path="/projects" name="projects" component={Addproject} />
          <Route exact path="/users" name="users" component={Adduser} />
          <Route exact path="/scope" name="scope" component={Addscope} />
          <Route exact path="/view-indiv-project" name="view-proj" component={ViewIndivProj} />
        </Switch>
      </Router>
    );
  }

  export default App;
